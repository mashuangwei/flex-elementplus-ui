import request from '@/utils/request'

// 查询学生信息表列表
export function listStudent(query) {
  return request({
    url: '/mf/student/list',
    method: 'get',
    params: query
  })
}

// 查询学生信息表详细
export function getStudent(studentId) {
  return request({
    url: '/mf/student/' + studentId,
    method: 'get'
  })
}

// 新增学生信息表
export function addStudent(data) {
  return request({
    url: '/mf/student',
    method: 'post',
    data: data
  })
}

// 修改学生信息表
export function updateStudent(data) {
  return request({
    url: '/mf/student',
    method: 'put',
    data: data
  })
}

// 删除学生信息表
export function delStudent(studentId) {
  return request({
    url: '/mf/student/' + studentId,
    method: 'delete'
  })
}
